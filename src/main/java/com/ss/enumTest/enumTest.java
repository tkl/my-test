package com.ss.enumTest;

import cn.hutool.setting.dialect.Props;
import cn.hutool.setting.dialect.PropsUtil;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class enumTest {

    public static void main(String[] args) throws UnsupportedEncodingException {
        for(WeekDay day:WeekDay.values()) {
            System.out.println(day+"====>"+day.getDay());
        }
//        WeekDay.printDay(5);
        System.out.println(WeekDay.Fri.getDay());

        Props props = PropsUtil.get("i18n/ExcelModel.properties");
        String str = new String(props.getStr("cbsEquipment_m").getBytes("ISO-8859-1"),"UTF-8");

        System.out.println(str);

    }
}
