package com.ss.enumTest;

import com.ss.bean.Student;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {
        Student student = new Student();
        student.setId("1");
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Student s = student.clone();
            s.setName("nana" + i);
            studentList.add(s);
        }
        studentList.stream().forEach(x -> {
            System.out.println(x.getName());
        });
    }
}
