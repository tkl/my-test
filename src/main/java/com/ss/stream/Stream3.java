package com.ss.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * 聚合（max/min/count)
 */
public class Stream3 {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("adnm", "admmt", "pot", "xbangd", "weoujgsd");

        Optional<String> max1 = list.stream().max(Comparator.comparing(String::length));
        System.out.println("最长的字符串：" + max1.get());
        //----------------------------------------------------------
        List<Integer> list2 = Arrays.asList(7, 6, 9, 4, 11, 6);
        // 自然排序
        Optional<Integer> max = list2.stream().max(Integer::compareTo);
        // 自定义排序
        Optional<Integer> max2 = list2.stream().max(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.println("自然排序的最大值：" + max.get());
        System.out.println("自定义排序的最大值：" + max2.get());
    }
}
